﻿# Archive Client
1. Assurez-vous que vous avez Node.js installés globalement. 
	- Sinon, télécharger-le sur ce site: https://nodejs.org/en/download/
	
2. Installer aussi Mongodb pour sauvegarder les résultats de recherche: https://www.mongodb.org/downloads#production
	- Voir le fichier database/config.js et modifier le lien vers le serveur mongodb si necessaire.
3. Lancer elasticSearch : https://www.elastic.co/fr/downloads/elasticsearch:
	- Voir le fichier database/config.js et modifier le lien vers le serveur elasticSearch si nécessaire. 
4. Voir le fichier database/config.js et modifier le lien vers le serveur contenant le médiateur si necessaire.
	
5. Lancer le site:
	- Lancer server: Aller dans le dossier /bin du projet, ouvrir le terminal et taper: node www
	- Lancer navigateur et aller http://localhost:3000

