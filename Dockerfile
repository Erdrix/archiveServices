FROM node:argon
RUN mkdir src
COPY . /src
WORKDIR /src
RUN npm install
EXPOSE 3000
CMD ["node", "/src/bin/www"]
