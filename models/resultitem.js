﻿var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SearchResult = new Schema({
    archive_id: {
    	type : String,
    	required : true,
    	unique : true,
    	dropDups: true
    },
    publication : Object, 
    date: Date
});

module.exports = mongoose.model('Result', SearchResult);