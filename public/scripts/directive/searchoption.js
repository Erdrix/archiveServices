/**
 * Created by Thais on 17/01/2016.
 */

angular.module('yapp').directive('searchOption', function() {
  return {
    templateUrl: 'scripts/directive/searchoption.html',
    restrict: 'E',
    replace: true,
  };
});