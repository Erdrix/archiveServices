'use strict';

/**
 * @ngdoc function
 * @name yapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of yapp
 */
angular.module('yapp')
  .controller('LoginCtrl', function($scope, $location, $http, searchService) {
    $scope.searchtext = '';
    $scope.searchResult = {};
    $scope.options = '1';
    $scope.searchOptions = [
      { searchtext: 'quafafou', option: 'author' }
    ];
    $scope.response = {};
    $scope.sources = ["hal", "arxiv"];

    getParameters();
    function getParameters(){
      searchService.getParameters(function(data){
        console.log(JSON.stringify(data.data.mapped));
        $scope.response = data.data;
      });
    }
    
    $scope.addOption = function(){
      $scope.searchOptions.push({
        searchtext: '', option: 'default'
      });
    }

    $scope.removeOption = function (index) {
      if (index > 0)
        $scope.searchOptions.splice(index, 1);
    }

    $scope.search = function() {
      $scope.searchResult = {};
      $scope.inProcessing = true;

      searchService.putSearchOptions($scope.searchOptions);
      searchService.putSearchSources($scope.sources);
      $location.url('/dashboard');
    }

     $scope.onSourceCheck = function($event, id){
        var checkbox = $event.target;
        console.log(id);
        console.log(checkbox.checked);  
        if(checkbox.checked)
            $scope.sources.push(id);
        else{
            var i;
            for(i=0; i < $scope.sources.length; i++){
                if($scope.sources[i] == id)
                    $scope.sources.splice(i, 1);
            }
        }
    }

  });
