'use strict';

/**
 * @ngdoc function
 * @name yapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of yapp
 */
angular.module('yapp')
  .controller('DashboardCtrl', function($scope, $state, $location, searchService) {
    $scope.searchResult = {};
    $scope.$state = $state;
    $scope.inProcessing = true;
    $scope.isCollapsed = true;
    $scope.toSave = [];

    $scope.searchOptions = searchService.getOptions();
    $scope.searchHistoryIds = searchService.getHistoriesIds();
    $scope.sources = ["hal", "arxiv"];

    if ($scope.searchOptions.length == 0){
          $scope.searchOptions.push(
              { searchtext: '', option: 'default' }
        );
    }

    $scope.onSourceCheck = function($event, id){
        var checkbox = $event.target;
        console.log(id);
        console.log(checkbox.checked);  
        if(checkbox.checked)
            $scope.sources.push(id);
        else{
            var i;
            for(i=0; i < $scope.sources.length; i++){
                if($scope.sources[i] == id)
                    $scope.sources.splice(i, 1);
            }
        }
    }

    // Get Parameters
    $scope.response = {};
    getParameters();
    function getParameters(){
      searchService.getParameters(function(data){
        console.log(JSON.stringify(data.data.mapped));
        $scope.response = data.data;
      });
    }

    $scope.addOption = function(){
      $scope.searchOptions.push({
          searchtext: '', option: 'text'
      });
    }

    $scope.removeOption = function (index) {
      if (index > 0)
          $scope.searchOptions.splice(index, 1);
    }

    $scope.onClickCheck = function($event, id){
        var checkbox = $event.target;
        console.log(id);
        console.log(checkbox.checked);  
        if(checkbox.checked)
            $scope.toSave.push(id);
        else{
            var i;
            for(i=0; i < $scope.toSave.length; i++){
                if($scope.toSave[i] == id)
                    $scope.toSave.splice(i, 1);
            }
        }
        console.log($scope.toSave);
    }

    $scope.search = function() {
        $scope.searchResult = {};
        $scope.inProcessing = true;

        searchService.putSearchOptions($scope.searchOptions);
        searchService.putSearchSources($scope.sources);

        searchService.getSearchResult(function(data){
            console.log('re-search result');
            $scope.searchResult = data.data;
            $scope.inProcessing = false;
        });
        return false;
    }

    searchService.getSearchResult(function(data){
        console.log('get search result');
          $scope.searchResult = data.data;
          $scope.inProcessing = false;
    });

    $scope.histories = {};
    

    $scope.searchIntoHistory = function(){
        // Prepare request to ElasticSearc
        $scope.searchResult = {};
        $scope.inProcessing = true;
        
        searchService.putSearchHistoriesOptions($scope.searchOptions);
        
        // Request ElasticSearch
        searchService.getSearchHistoriesResult(function(data){
            console.log("Histories :"+JSON.stringify(data));
            $scope.histories = data;            
        });
    }

    $scope.save = function(){
        console.log($scope.toSave);
        searchService.putToSave($scope.toSave);
        searchService.saveResult(function (data){
            if (data.success){
                $location.url('dashboard/reports');
            }
            else{
                alert('Sauvegarde impossible: ' + data.msg);
            }
        });
    }
  });
