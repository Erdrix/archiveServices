angular.module('yapp').factory('searchService', ['$http', '$location', function($http, $location) {
  var service = {
    getHistories: getHistories,
    getHistoryResults: getHistoryResults,
    putSearchOptions: setSearchOption,
    putSearchHistoriesOptions: setSearchHistoriesOption,
    putToSave: setToSave,
    getSearchResult: getResult,
    getSearchHistoriesResult: getHistoriesResult,
    getOptions: getOptions,
    saveResult: saveResult, 
    getParameters: getParameters, 
    getHistoriesIds: getHistoriesIds,
    setHistoriesIds: setHistoriesIds,
    putSearchSources: setSearchSources
  };
  var archiveBusUrl = 'http://localhost:9000/v1';
  var elasticUrl = 'http://localhost:9200';
  var searchOptions = [];
  var toSave=[];
  var searchHistoriesOptions = [];
  var searchHistoryIds = [];
  var searchResults = {};
  var searchHistoriesResults = {};
  var parameters = {};
  var searchSources = [];


  return service;
  function setSearchSources(data){
    searchSources = data;
  }

  function setSearchOption(data){
    searchOptions = data;
  }

  function setToSave(data){
    toSave = data;
  }

  function getOptions(){
    return searchOptions;
  }

  function setSearchHistoriesOption(data){
    searchHistoriesOptions = data;
  }

  function getHistoriesIds(){
    return searchHistoryIds;
  }

  function setHistoriesIds(data){
    searchHistoryIds = data;
  }

  function getHistories(callback){
    $http.get('/api/histories')
        .success(function(data, status, headers, config){
            return callback(data);
        });
  }

  function getHistoryResults(id, callback){
    $http.get('/api/histories/' + id)
        .success(function(data, status, headers, config){
          console.log(JSON.stringify(data));
          return callback(data);
        });
  }

  function saveResult(callback){
    var data = {};
    data.options = searchOptions;
    data.nbPublications = searchResults.data.nbPublications;
    data.results = [];
    
    angular.forEach(searchResults.data.publications, function(pub){
      if(toSave.includes(pub.id[0]))
        data.results.push(pub);
    });

    $http.post('/api/save', data)
        .success(function(data, status, headers, config) {
            return callback(data);
        })
        .error(function(data, status, headers, config) {
          return callback(null);
        });
  }

  function getResult(callback){
    console.log(archiveBusUrl);
    console.log(searchOptions);
    var data = {'parameters':{}};
    var founded = [];
    angular.forEach(searchOptions, function(option){
        var opt = option.option;
        if(!founded.includes(opt)){
          data.parameters[opt] = [option.searchtext];
          founded.push(opt);
        }
        else {
          data.parameters[opt].push(option.searchtext);
        }
    });
    data.maxSize = 10;
    data.start = 0;
    data.sources = searchSources;

    var req = {
      method : 'POST',
      url: archiveBusUrl+'/publications',
      headers:{
        'Content-Type': 'application/json'
      },
      data: data
    }
    $http(req).then(function(data, status, headers, config){
      console.log(JSON.stringify(data));
      searchResults = data;
      callback(data);
    });
  }

  function getHistoriesResult(callback){
    var data = {'query':{'query_string':{}}};
    data.query.query_string.query = "";
    angular.forEach(searchHistoriesOptions, function(option){
        var opt = option.option;
        if(opt == 'default')
          data.query.query_string.query += option.searchtext+"~ AND "; 
        else 
          data.query.query_string.query += option.option+":"+option.searchtext+"~ AND ";  

    });
    data.query.query_string.query = data.query.query_string['query'].substr(0, data.query.query_string['query'].length-5);
    var req = {
      method : 'POST',
      url: elasticUrl+'/archive/publications/_search',
      headers:{
        'Content-Type': 'application/json'
      },
      data: data
    }
    $http(req).then(function(data, status, headers, config){
      searchHistoriesResults = data;
      extractList(data,callback);
    });
  }

  function extractList(data, callback){
    //var founded = [];
    var publications = [];
    console.log(data);
    angular.forEach(data.data.hits.hits, function(hit){
      publications.push(hit._source);
    });
    callback(publications);
  }

  function getParameters(callback){
      $http.get(archiveBusUrl+'/parameters')
          .then(function(data, status, headers, config){
              return callback(data);
          });
    }
}]);
