var express = require('express');
var router = express.Router();
var SearchResult = require('../models/resultitem.js');
var config = require('../database/config.js');
var request = require('request');
var foreach = require('async-foreach').forEach;

router.post('/save', function (req, res) {
    var searchNbFound = req.body.nbPublications;
    var searchOptions = req.body.options;
    var searchResults = req.body.results;
    foreach(searchResults, function(item, index, arr){

         // On prépare ce que l'on va stocker dans mongo
        var result = new SearchResult();
        result.date = new Date();
        result.publication = item;
        result.archive_id = item.id[0];
            
        // On prepare ce que l'on va stocker dans elasticSearch
        result.save(function (err, r, numA) {
            if (err)
                console.log(err);//res.json({ success: false, msg: 'Can not save search resultat' });
            else{
                var data = {};
                data = item;
                data.id = result._id;
                console.log(data);
                request({
                    url : config.elasticsearch+'/publications',
                    method : 'POST',
                    json: data}, 
                    function(error, response, body){
                        if(error)
                            console.log(error);});
            }
        });
        

    }); 
    res.json({ success: true, msg: 'Save ok' });
});



module.exports = router;